from django.db import models

# Create your models here.




class DateTimeMixin(models.Model):
    create_at = models.DateTimeField(auto_now_add = True)
    update_at = models.DateTimeField(auto_now = True)

meeting_choice = (
    (1,"Instant Meeting"),
    (2,"Schedule Meeting"),
)

class ZoomMeetingIntegration(DateTimeMixin):
    topic = models.CharField(max_length = 250, blank=True, null = True)
    duration = models.IntegerField(null = True, blank = True)
    type = models.CharField(max_length = 5, choices = meeting_choice, null = True, blank = True)
    start_time = models.DateTimeField(null = True, blank = True)
    agenda = models.CharField(max_length = 300, null = True, blank = True)
    url = models.TextField(null = True, blank = True)
    password = models.CharField(max_length = 500, null = True, blank = True)

    def __str__(self) -> str:
        return str(self.topic)

    # def validation_error(self):
    #     return    


