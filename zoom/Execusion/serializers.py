from rest_framework import serializers
from .models import *

class ZoomMeetingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ZoomMeetingIntegration
        fields = "__all__"

    def validate(self, data):

        if data['topic'] is None :
            raise serializers.ValidationError("Not More than 50")    
        elif len(data['topic']) > 50 or len(data['agenda']) > 50:
            raise serializers.ValidationError("Topic Is Compulsory")
        return data    


        # if len(data['agenta']) > 50:
        #     raise 


