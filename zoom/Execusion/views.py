from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.status import *

from .serializers import *
import jwt
import requests
import json
from time import time

from rest_framework.decorators import api_view
from rest_framework.decorators import action

# Enter your API key and your API secret
API_KEY = 'Y9XgDYBuSC218_NebY7AsQ'
API_SEC = '8EPkX64tisN8kMwWfZyaFBAOCmj0RGKRXKnM'

# create a function to generate a token
# using the pyjwt library


def generateToken():
	token = jwt.encode(
		{'iss': API_KEY, 'exp': time() + 5000},
		API_SEC,
		algorithm='HS256'
	    )
	return token




def meetingdetails(data):
    return  {"topic": data['topic'],
				"type": data['type'],
				"duration": data['duration'],
				"timezone": "Asia/Calcutta",
				"agenda": data['agenda'],
                "password": data['password'],

				"recurrence": {"type": 1,
								"repeat_interval": 1
								},
				"settings": {"host_video": "true",
							"participant_video": "true",
							"join_before_host": "False",
							"mute_upon_entry": "False",
							"watermark": "true",
							"audio": "voip",
							"auto_recording": "cloud"
							}
				}


# @api_view(['GET'])
# def createmeeting(request):
#     headers = {'authorization': 'Bearer ' + generateToken(),
# 			'content-type': 'application/json'}
#     r = requests.post(
# 		f'https://api.zoom.us/v2/users/me/meetings',
# 		headers=headers, data=json.dumps(meetingdetails))
#     data = json.loads(r.text)

#     return Response(data)



class ZoomMeetingViewSet(viewsets.ModelViewSet):
    queryset = ZoomMeetingIntegration.objects.all()
    serializer_class = ZoomMeetingSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data = request.data)


        if serializer.is_valid():
            headers = {'authorization': 'Bearer ' + generateToken(),
                'content-type': 'application/json'}
            r = requests.post(
                f'https://api.zoom.us/v2/users/me/meetings',
                headers=headers, data=json.dumps(meetingdetails(request.data)))
            data = json.loads(r.text)
            serializer.save(url = data['join_url'])
            return Response(data, status=HTTP_200_OK)
        return Response(serializer.errors)

    # @action(methods=['GET'], detail=True)
    # def transform(self, *args, **kwargs):

    #     print(self.get_object())


    #     return Response("Yup")    
